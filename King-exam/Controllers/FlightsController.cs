﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using King_exam.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace King_exam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightsController : ControllerBase
    {
        private readonly Dictionary<SearchParamsModel, IEnumerable<FlightModel>> _searches;
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public FlightsController(HttpClient httpClient, IConfiguration configuration, Dictionary<SearchParamsModel, IEnumerable<FlightModel>> searches)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _searches = searches;
        }


        // GET api/flights
        [HttpGet]
        public async Task<ActionResult<string>> GetAccessToken()
        {
            var values = new Dictionary<string, string>
                {
                    { "grant_type", "client_credentials" },
                    { "client_id", _configuration.GetValue<string>("Amadeus:ClientId")},
                    { "client_secret", _configuration.GetValue<string>("Amadeus:ClientSecret")}
                };
            var content = new FormUrlEncodedContent(values);

            try
            {
                HttpResponseMessage response = await _httpClient.PostAsync(
                    "https://test.api.amadeus.com/v1/security/oauth2/token",
                    content);

                var responseString = await response.Content.ReadAsStringAsync();
                var tokenModel = JsonConvert.DeserializeObject<TokenResponseModel>(responseString);

                return StatusCode(200, tokenModel.Access_Token);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<string>> SearchFlights([FromBody] SearchParamsModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(400, "Search query invalid.");
            }

            // sets default adults and currency value for optimizing dictionary
            if (!model.Adults.HasValue)
                model.Adults = int.Parse(_configuration.GetValue<string>("Constants:NoOfPassengers"));

            if (string.IsNullOrEmpty(model.Currency))
                model.Currency = _configuration.GetValue<string>("Constants:Currency");

            // check if query is in the local storage
            var cachedFlights = FindCachedFlights(model);
            if (cachedFlights != null)
                return StatusCode(200, cachedFlights);

            var auth = new AuthenticationHeaderValue("Bearer", model.Token);
            var uri = "https://test.api.amadeus.com/v1/shopping/flight-offers";
            // get query as string from view model
            var query = model.GetQuery();

            try
            {
                _httpClient.DefaultRequestHeaders.Authorization = auth;
                HttpResponseMessage response = await _httpClient.GetAsync(
                    new Uri(uri + query));
                _httpClient.DefaultRequestHeaders.Authorization = null;

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return StatusCode(401, "Get new token");
                }

                var responseString = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<FlightsResponseModel>(responseString);

                if (data.Data == null || data.Data.Count() == 0)
                {
                    return StatusCode(204, "No flights found matching your criteria");
                }

                var flights = data.Data.Select(x => new FlightModel(x, model.Adults.Value, model.Currency,
                    data.Dictionaries.Locations));
                _searches.Add(model, flights);

                return StatusCode(200, flights);
            }
            catch (Exception e)
            {
                _httpClient.DefaultRequestHeaders.Authorization = null;

                return StatusCode(500, e.Message);
            }
        }

        #region helpers
        private IEnumerable<FlightModel> FindCachedFlights(SearchParamsModel model)
        {
            foreach (KeyValuePair<SearchParamsModel, IEnumerable<FlightModel>> entry in _searches)
            {
                if (entry.Key.Equals(model)) return entry.Value;
            }

            return null;
        }
        #endregion
    }
}
