﻿using King_exam.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace King_exam.Attributes
{
    public class CurrencyAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            var model = (SearchParamsModel)validationContext.ObjectInstance;
            var modelCurrency = model.Currency;
            string[] allowerCurrencies = { "EUR", "HRK", "USD" };

            if (string.IsNullOrEmpty(modelCurrency) || allowerCurrencies.Contains(modelCurrency))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Currency must be either EUR, HRK or USD.");
        }
    }
}
