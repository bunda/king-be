﻿using King_exam.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace King_exam.ViewModels
{
    public class FlightModel : SearchParamsModel
    {
        public int DepartureNoOfStopovers { get; set; }
        public int? ReturnNoOfStopovers { get; set; }
        public decimal Price { get; set; }


        public FlightModel()
        {
        }

        public FlightModel(FlightItemModel flight, int adults, string currency, Dictionary<string, Location> locations)
        {
            // if number of services == 1 => one way trip
            // if number of services == 2 => return type trip

            // number of segments is the number of stopovers
            DepartureNoOfStopovers = flight.OfferItems.ElementAt(0)
                .Services.ElementAt(0).Segments.Count();

            if (flight.OfferItems.ElementAt(0).Services.Count() == 2)
            {
                ReturnNoOfStopovers = flight.OfferItems.ElementAt(0)
                    .Services.ElementAt(1).Segments.Count();
                ReturnDate = flight.OfferItems.ElementAt(0).Services.ElementAt(1)
                .Segments.ElementAt(0).FlightSegment.Departure.At;
            }

            DepartureDate = flight.OfferItems.ElementAt(0).Services.ElementAt(0)
                .Segments.ElementAt(0).FlightSegment.Departure.At;
            Price = flight.OfferItems.ElementAt(0).Price.Total;

            Origin = "";
            Destination = "";
            var origin = locations[flight.OfferItems.ElementAt(0).Services.ElementAt(0)
                .Segments.ElementAt(0).FlightSegment.Departure.IataCode];
            var destination = locations[flight.OfferItems.ElementAt(0).Services.ElementAt(0).Segments.
                ElementAt(DepartureNoOfStopovers - 1).FlightSegment.Arrival.IataCode];

            if (origin != null)
            {
                Origin = origin.DetailedName;
            }
            if (origin != null)
            {
                Destination = destination.DetailedName;
            }


            Adults = adults;
            Currency = currency;
        }

    }
}
