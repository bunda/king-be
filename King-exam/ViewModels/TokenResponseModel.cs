﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace King_exam.ViewModels
{
    public class TokenResponseModel
    {
        public string Type { get; set; }
        public string Username { get; set; }
        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public string Access_Token { get; set; }
    }
}
