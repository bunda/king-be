﻿using King_exam.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace King_exam.ViewModels
{
    public class FlightsResponseModel
    {
        public MetaResponse Meta { get; set; }
        public List<FlightItemModel> Data { get; set; }
        public JsonDictionary Dictionaries { get; set; }
        //public Dictionary<string, Dictionary<string, dynamic>> Dictionaries { get; set; }
    }

    public class JsonDictionary
    {
        public Dictionary<string, Location> Locations { get; set; }
    }

    public class Location
    {
        public string SubType { get; set; }
        public string DetailedName { get; set; }
    }

    public class MetaResponse
    {
        public string Currency { get; set; }
    }

    public class FlightItemModel
    {
        public string Type { get; set; }
        public string Id { get; set; }

        public List<OfferItem> OfferItems { get; set; }
    }

    public class OfferItem
    {
        // njih je vise ako je povratno putovanje u pitanju
        public List<ServiceItem> Services { get; set; }
        public Price Price { get; set; }
        public Price PricePerAdult { get; set; }
    }

    public class Price
    {
        public decimal Total { get; set; }
        public decimal TotalTaxes { get; set; }
    }

    public class ServiceItem
    {
        // broj ovoga je broj presjedanja
        public List<SegmentItem> Segments { get; set; }
    }

    public class SegmentItem
    {
        public FlightSegmentItem FlightSegment { get; set; }
    }

    public class FlightSegmentItem
    {
        public Airport Departure { get; set; }
        public Airport Arrival { get; set; }
    }

    public class Airport
    {
        public string IataCode { get; set; }
        public string Terminal { get; set; }
        public DateTime At { get; set; }
    }
}
