﻿using King_exam.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace King_exam.ViewModels
{
    public class SearchParamsModel
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string Origin { get; set; }
        [Required]
        public string Destination { get; set; }
        [Required]
        public DateTime DepartureDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        [Range(1, 50)]
        public int? Adults { get; set; }
        [Currency]
        public string Currency { get; set; }

        public string GetQuery()
        {
            var query = "?origin=" + Origin + "&destination="
                + Destination + "&departureDate=" + DepartureDate.ToString("yyyy-MM-dd")
                + "&adults=" + Adults.Value + "&currency=" + Currency;

            if (ReturnDate.HasValue)
                query += "&returnDate=" + ReturnDate.Value.ToString("yyyy-MM-dd");

            return query;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SearchParamsModel)) return false;

            var paramsModel = (SearchParamsModel)obj;

            if (!Origin.Equals(paramsModel.Origin)) return false;
            if (!Destination.Equals(paramsModel.Destination)) return false;
            if (!DepartureDate.Date.Equals(paramsModel.DepartureDate.Date)) return false;
            if (!Adults.Equals(paramsModel.Adults)) return false;
            if (!Currency.Equals(paramsModel.Currency)) return false;

            if (ReturnDate.HasValue != paramsModel.ReturnDate.HasValue) return false;
            if (ReturnDate.HasValue && paramsModel.ReturnDate.HasValue)
            {
                if (!ReturnDate.Value.Date.Equals(paramsModel.ReturnDate.Value.Date))
                    return false;
            }

            return true;
        }
    }
}
